<div align="center">

# **Game Design Document - A Queda de Seth**

![ALT](https://gitlab.com/StephNascimento/images_gdd/-/raw/main/logo-f2l-team.jpg)


![ALT](https://gitlab.com/lucasnazato/gdd_seth/-/raw/main/GDDImages/a-queda-de-seth-banner.jpg)

Para PC (GameJolt)

**Classificação:** ![ALT](https://gitlab.com/lucasnazato/gdd_seth/-/raw/main/GDDImages/ClassInd_14.png)

**Data de lançamento:** 08/06/2022

F2LTeam / f2lteamgames@gmail.com
</div>

## História do jogo:

O Deus Seth tomou controle do Egito Antigo e controla as terras como um tirano, obrigando os seus servos humanos a trabalharem exaustivamente. Você joga como Bak, filho do Deus Seth, e não concorda com as práticas do seu pai e busca a ajuda dos outros deuses para libertar a população.

## O jogo:
Em A Queda de Seth, o jogador toma controle de Bak, lutando contra os sacerdotes de seu pai, o Deus Seth, enquanto progride pelo seu templo. O jogo é separado tem quatro fases, duas fases são para que o jogador se acostume com as mecânicas. As duas fases últimas ele utilizará o que aprendeu durantes as fases anteriores para vencer o jogo.

## Fases:
### 1ª Fase:
O foco é dado para a movimentação do personagem e suas habilidades e como isso pode ser usado para progredir pelo templo, como coletar itens para abrir passagens e obstáculos.

<div align="center">

![ALT](https://gitlab.com/StephNascimento/images_gdd/-/raw/main/level1_3.png)
</div>

### 2ª Fase:
O jogador é introduzido ao combate do jogo contra inimigos, em um sistema de hordas, onde precisa sobreviver ao desafio para que possa progredir. São também introduzidas novas mecânicas de se locomover pelo nível.

<div align="center">

![ALT](https://gitlab.com/StephNascimento/images_gdd/-/raw/main/level2_2.png)
</div>

### 3ª e 4ª Fase:
Mecânicas de quebra-cabeça são introduzidas e o jogador tem de enfrentar mais inimigos, utilizando todas as habilidades que aprendeu até agora. Imagens dos níveis 3 e 4:

<div align="center">

![ALT](https://gitlab.com/StephNascimento/images_gdd/-/raw/main/level3_2.png)

![ALT](https://gitlab.com/StephNascimento/images_gdd/-/raw/main/level4_1.png)
</div>


## Ritmo:
Exploração de templos egípcios com combate usando magia.


## Personagem do jogador
**Bak:** Conforme o player avança no jogo e ganha o apoio de diferentes deuses, ele passa a ter acesso a diferentes habilidades que podem ajudar tanto no combate quanto na exploração do templo de seu pai. Neste protótipo o jogador já inicia com as habilidades básicas de movimento e a capaciade de flutuar, um ataque base e duas habilidades especiais de ataque.

<div align="center">

![ALT](https://gitlab.com/lucasnazato/gdd_seth/-/raw/main/GDDImages/Bak.png)
</div>


## Controles do jogador:

| Comandos | Keyboard/Mouse |
| ------ | ------ |
| Mover personagem | WASD |
| Mover câmera | Mouse |
| Pular | Barra de espaço |
| Correr | Shift esquerdo |
| Atirar | Botão esquerdo do mouse |
| Concentração Solar | 1 |
| Pedras Solares | 2 |
| Flutuar | 3 |

## Mundo do jogo:
O mundo de A Queda de Seth é inspirado na mitologia egípcia e portanto os seus visuais, level design e mecânicas são todos baseados em diferentes aspectos desta cultura. A principal localização que o jogador explora é o templo do Deus Seth, que se encontra no meio do deserto. Conforme o jogador prossegue pelo templo armadilhas e inimigos mais perigosos aparecem.
 
## Experiência de jogo:
O jogo é dividido em níveis para exploração, sendo que cada um destes é “dividido” em diferentes fases, Cutscene / Exploração / Puzzle / Combate, que podem estar em ordens diferentes.

**Cutscene** - O jogador recebe uma breve explicação da história e um objetivo que precisa ser cumprido.

**Exploração** - Percorrer o templo para achar itens de vida e dano, entre outras recompensas.

**Puzzle** - Em certos pontos o jogador irá precisar resolver puzzles ou achar os itens corretos para que ele possa progredir no nível.

**Combate** - Inimigos estão espalhados pelos níveis, podendo ser combates opcionais ou obrigatórios para a progressão.

**Como vencer** - Atravessar o templo e derrotar o Deus Seth.

**Como perder** - Ser derrotado por algum inimigo ou armadilha.

## Mecânicas de jogo
A Queda de Seth é um jogo em terceira pessoa, com a câmera posicionada atrás do personagem, podendo ser rotacionada em 360 graus. O jogador percorre o templo batalhando contra inimigos. As ações realizadas pelo jogador são as de coletar itens de melhoria (vida e dano) e itens de progressão (abrir portas), desviar de armadilhas, atirar magia para derrotar inimigos e interagir com o ambiente.

A HUD mostra ao jogador a sua vida e dano, no canto superior esquerdo, que são mantidas de um nível para o outro, e pode mostrar informações contextuais ao modo de jogo do momento, como o sistema de hordas, canto superior direito, onde as ondas e o tempo de início destas são destacados, canto superior central. E demonstra as habilidades do jogador, canto inferior esquerdo.

<div align="center">

![ALT](https://gitlab.com/StephNascimento/images_gdd/-/raw/main/Clipboard_Image.jpg)
</div>

## Power-ups e Habilidades
O jogador pode coletar dois tipos de power-up e tem três tipos de habilidade:

| Nome | O que faz | Valor |
| ------ | ------ | ------ |
| Ankh Dourado | Aumenta o dano causado pelo personagem que por padrão é 5 | 2 de dano|
| Ankh Prata | Recupera a vida faltante do personagem | 10 de vida|
| Concentração Solar | Abre um buraco negro que tira vida dos inimigos próximos | 15 de dano a cada meio segundo / aumenta em 2 de dano a cada ankh dourado|
| Pedras Solares | Caem pedras tirando vida dos inimigos que estiverem na frente do jogador | 10 de dano a cada meio segundo / aumenta em 1 de dano a cada ankh dourado| 
| Flutuar | Permite que o jogador fique planando | 5 segundos |

<div align="center">

![ALT](https://gitlab.com/lucasnazato/gdd_seth/-/raw/main/GDDImages/Itens.png)
</div>

## Inimigos:
Os inimigos presentes no templo são os sacerdotes de Seth, que podem ter habilidades diferentes.

**Seth:**
O inimigo principal do jogo, ele aparecerá no fim do jogo quando o jogador enfrentar todas as hordas do último nível.

<div align="center">

![ALT](https://gitlab.com/StephNascimento/images_gdd/-/raw/main/Screenshot_Sevarog_3_Chronos-min-1920x1080-5c94c01dcf9c7be7e5cb8617ae244fb2.jpg)
</div>

**Servo Revivido de Seth:** 
Este inimigo persegue o jogador e quando se aproxima ataca usando o seu machado. Seu ataque dá 10 de dano no jogador.

<div align="center">

![ALT](https://gitlab.com/StephNascimento/images_gdd/-/raw/main/unknown__3_.png)
</div>

**Regressada:** 
Se posiciona de forma que o jogador esteja no seu campo de visão e utiliza ataques a distância. Cada projétil tira 5 de vida do jogador.

<div align="center">

![ALT](https://gitlab.com/lucasnazato/gdd_seth/-/raw/main/GDDImages/Morigesh.png)
</div>

## Multiplayer:
O multiplayer não seguiria o mesmo formato do jogo principal, ele teria dois modos de jogo, co-op e versus.
No co-op os jogadores iriam colaborar entre si para vencer os inimigos que viriam em 4 hordas.
E no versus eles lutam em times ou cada um por si.

## Cenas de corte:
As cenas de corte serão feitas dentro jogo, usando itens/NPCs para expor a história e dar objetivos ao jogador.
No primeiro nível quando o jogador coleta as moedas com o olho de Hórus, uma cena de corte aparece mostrando as portas se abrindo, assim também quando ele coleta todos os ankhs que estão para demonstração no início do primeiro nível.

## Diferenciais:
- Temática na mitologia egípcia;
- Mecânicas de disparos mágicos;
- Gameplay rápida e divertida de enfrentar hordas de inimigos;
- O enredo e roteiro de história do jogo foram construídos com base no 10º Objetivo de Desenvolvimento Sustentável da ONU.

<div align="center">

![ALT](https://gitlab.com/StephNascimento/images_gdd/-/raw/main/odssss-1-scaled.jpg)
</div>

## Créditos:
- Utilizado assets gratuitos para uso do Quixel Bridge e assets com licença livre do SketchFab.
- Os sons e músicas utilizado dentro do jogo foram pegos de forma gratuita no FreeSound e também músicas sem Copyright do Youtube.
